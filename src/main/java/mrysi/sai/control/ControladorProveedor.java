/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mrysi.sai.control;
import java.util.List;
import mrysi.sai.entidades.Proveedor;
import mrysi.sai.oad.OadProveedor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/proveedor")
public class ControladorProveedor {
    private final static Logger LOG = LoggerFactory.getLogger(ControladorProveedor.class);
     
    @Autowired
    OadProveedor oadProveedor;
    
    @GetMapping("")
    public List<Proveedor> getTodosProveedor() {
        LOG.debug("GET");
        return oadProveedor.findAll();
    }
    @GetMapping("/{id}")
    public Proveedor getProveedor(@PathVariable("id") Integer id) {
        LOG.debug("GET {}", id);
        return oadProveedor.findOne(id);
        // return (Libro) oadLibro.usuarioEspecifico(id);
    }
  
     @PutMapping("/{id}")
    public Proveedor actualizarPropuestaTecnica(@PathVariable("id") Integer id, @RequestBody Proveedor pro) {
        LOG.debug("PUT {}", pro);
        pro.setIdProveedor(id);
        oadProveedor.save(pro);
        return pro;
    }
    
}
